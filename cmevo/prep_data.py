import pandas as pd
from os import listdir, path
from io import StringIO

# Path to read the data from
BASE_DATA_PATH = path.join('data', 'raw')

# Columns for the meta data
COLUMNS = ('DATE-OBS', 'TIME-OBS', 'DETECTOR', 'FILTER', 'OBSERVER', 'FEAT_CODE', 'IMAGE_TYPE',
           'YHT_ID', 'ORIG_HTFILE', 'ORIG_WDFILE', 'UNIVERSAL', 'WDATA', 'WDATA', 'HALO', 'ONSET1',
           'ONSET2', 'ONSET2', 'CEN_PA', 'WIDTH', 'SPEED', 'ACCEL', 'FEAT_PA', 'FEAT_QUAL',
           'QUALITY_INDEX', 'REMARK', 'COMMENT', 'NUM_DATA_POINTS', 'HT_DATA')


def parse_CDAW_data(cdaw_lines):
    """
    Parses the meta data and height time data from a CDAW entry into a pandas data frame.

    Parameters
    ----------
    lines array The lines of the CDAW file

    """
    meta_data = cdaw_lines[1:27]
    meta_data = [element.split(': ')[1][:-1] for element in meta_data]
    height_time_data = cdaw_lines[27:]
    ht_data = pd.read_csv(StringIO(''.join(height_time_data)),
                          sep='\s+', comment='#',
                          names=['HEIGHT', 'DATE', 'TIME', 'ANGLE', 'TEL', 'FC', 'COL', 'ROW'],
                          parse_dates=[[1, 2]])

    meta_data.append(len(ht_data))
    meta_data.append(ht_data)
    return meta_data


if __name__ == '__main__':
    # List all the data files
    file_names = listdir(BASE_DATA_PATH)

    # Create data frame to hold the data
    df = pd.DataFrame(columns=COLUMNS)

    # Loop over files
    for file_name in file_names:
        with open(BASE_DATA_PATH + file_name) as file:
            lines = file.readlines()
            data = parse_CDAW_data(lines)
            cme_count = len(df)
            df.loc[cme_count] = data
            if cme_count % 1000 == 0:
                print(cme_count)

    df.to_pickle(path.join('data', 'all_cmes.pkl'))
