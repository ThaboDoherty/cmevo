"""Summary
"""
import numpy as np
import matplotlib.pyplot as plt
import os
import errno
from kiniematic_fits import sin_vel_model, quad_vel_model


# Function to make create or replace new folder
try:
    os.makedirs(os.path.join('..', 'Figures', 'Events'))
except OSError as exception:
    if exception.errno != errno.EEXIST:
        raise


# Create Function that Limits range of parameters
def plot_hist(x, lower, upper, n_bins, title, units, plot):
    """Summary
    
    Args:
        x (TYPE): Description
        lower (TYPE): Description
        upper (TYPE): Description
        n_bins (TYPE): Description
        title (TYPE): Description
        units (TYPE): Description
        plot (TYPE): Description
    
    Returns:
        TYPE: Description
    """
    x = x[x < upper]
    x = x[x > lower]
    # Creates the histogram values
    bins, edge = np.histogram(x, n_bins, normed=True)
    width = np.diff(edge)
    # Gets max value of the histogram
    center = (edge[0:-1] + edge[1:])/2
    maxindex = np.argmax(bins)
    if plot == 1:
        # Plots Histogram
        plt.bar(edge[0:-1], bins, align='edge', width=width[0])
        plt.title(title)
        plt.xlabel('The most common {} is {} {}'.format(title, center[maxindex], units))
    return center[maxindex], bins[maxindex]


if __name__ == '__main__':
    for filename in os.listdir(os.path.join('..', 'data', 'Parameters')):
        data = np.load(os.path.join('..', 'data', 'Parameters', '{}'.format(filename)))
        
        # Getting all the relevant values and arrays from the file
        res = data['res']
        n_bins = data['n_bins']
        Counts = data['Counts']
        t_diff = data['t_diff']
        v = data['v']/1000
        quad_opt = data['quad_opt']
        min_period = 60*60
        
        # Creating the figure that all the plots will be on
        fig = plt.figure(figsize=(15, 12))
        gs = plt.GridSpec(3, 2)
        
        # Amplitude
        plt.subplot(gs[1, 0])
        Ampmax, Ampind = plot_hist(res[:,0]/1000, 0, 400, n_bins, 'Amplitude', 'km/s', 1)
        
        # Period
        plt.subplot(gs[1, 1])
        Permax, Perind = plot_hist(res[:,1]/60, min_period/60, t_diff[-1]/60, n_bins, 'Period', 'Minutes', 1)

        # Phase
        Phasemax, Phaseind = plot_hist(res[:,2], 0, 2*np.pi, n_bins, 'Phase', 'rads', 0)
 
        # Velocity
        plt.subplot(gs[2, 0])
        Velmax, Velind = plot_hist(res[:,3]/1000, 10, 4000, n_bins, 'Velocity', 'km/s', 1)

        # Acceleration
        plt.subplot(gs[2, 1])
        Accmax, Accind = plot_hist(res[:,4], -50, 50, n_bins, 'Acceleration', 'm/s**2', 1)

        # Creating plot of initial data
        plt.subplot(gs[0, :])
        plt.scatter(t_diff/60, v * 60, color='red', label='_nolegend_')
        
        # Change to right units
        # Period(km/s) to (km/m)
        Ampmax * 60=Permax

        # Period(m)

        # Phasemax(rads)

        # Vel=(km/s) to (km/m)
        Velmax * 60

        # Acceleration(m/s**2) to (km/m**2)
        Accmax / 1000 * 60 * 60 

        # Overplotting fitted function
        V0 = (Ampmax, Permax, Phasemax, Velmax, Accmax)
        X = np.linspace(0, t_diff[-1]/60, 1000)
        Sinfit = plt.plot(X, sin_vel_model(X, *V0), label='Sine Fit')
        
        # Overplot Quadratic Function
        VelAcc = plt.plot(X, quad_vel_model(X, *quad_opt[1:]), color='Green', label='Quadfit')
        Quadfit = plt.plot(X, quad_vel_model(X, *quad_opt[1:]), alpha=0, color='Green', label='Velocity={}km/s Acc={}m/s**2'.format(np.round(quad_opt[1], 0), np.round(quad_opt[2],2)))
        fig.subplots_adjust(hspace=1)
        plt.legend()
        plt.text(t_diff[-1]/120, np.min(v), 'No. of Successful Fits={}'.format(n_bins**2-Counts))
        plt.tight_layout()
        
        # Save figure in folder
        fig.savefig(os.path.join('..', 'Figures', 'Events', '{}.png'.format(filename.split('.')[0])))
        print('Done {}'.format(filename.split('.')[0]))
        plt.close()
