from os import path, getcwd

from ..prep_data import parse_CDAW_data


class TestPrepData(object):
    def test_parse_CDAW_data(self):
        print(getcwd())
        with open(path.join('cmevo', 'tests', 'data', '20121207.213605.w165p.v0723.p283g.yht')) as file:
            lines = file.readlines()
            data = parse_CDAW_data(lines)

            assert data[0] == '2012/12/07'  # Date obs
            assert data[1] == '21:36:05'    # Time obs
            assert data[6] == 'RUNNING DIFF'
            assert data[23] == '2 (Fair)'   # Quality
