import numpy as np

from ..kiniematic_fits import derivative


class TestKinematicFits(object):
    def test_derivative(self):
        x = y = np.linspace(1, 10, 10)

        deriv = derivative(x, y)
        assert np.array_equal(deriv[1], np.ones((9)))
