import pickle
import pandas as pd
import numpy as np
import errno
import os
from multiprocessing import Pool, cpu_count
from datetime import datetime
from scipy.optimize import curve_fit

RSUN = 6.957e8  # Meters
# Function to make create or replace new folder
try:
    os.makedirs(os.path.join('..', 'data', 'Parameters'))
except OSError as exception:
    if exception.errno != errno.EEXIST:
        raise


# Define function to get the maximum value of the histogram and the size of the bin.
def histogram_max(bins, edge):
    center = (edge[0:-1] + edge[1:])/2
    maxindex = np.argmax(bins)
    return center[maxindex], bins[maxindex]


# Making new matrix with only relevant data
def filter_cmes(date_min, date_max, min_ht):
    # Unpacking the data in the pickle file
    with open('../data/all_cmes.pkl', 'rb') as f:
        all_cmes = pickle.load(f)
    # Using Pandas and date time to convert the time of each observation to datetime
    cme_datetimes = pd.to_datetime(all_cmes['DATE-OBS'] + ' ' + all_cmes['TIME-OBS'])
    all_cmes = all_cmes.assign(datetime=cme_datetimes)
    # Getting rid of all the CMEs that dont have enough points to study
    filtered_cmes = all_cmes[
        (all_cmes['NUM_DATA_POINTS'] >= min_ht) & (all_cmes['datetime'] >= date_min) &
        (all_cmes['datetime'] <= date_max)]
    return filtered_cmes


# Making the function that derives the velocity using height and time
# Returns the halfway point between the points in question and the slope
def derivative(x, y):
    xd = np.diff(x, 1)
    yd = np.diff(y, 1)
    
    deriv = yd / xd
    xx = x[:-1] + xd / 2.0
    
    xx = np.delete(xd, np.argwhere(np.isinf(xd)))
    deriv = np.delete(xd, np.argwhere(np.isinf(xd)))

    return xx, deriv


# Functions to create linear and quadratic fits for height and velocity

def lin_height_model(t, *a):
    return a[0] + a[1] * t


def quad_height_model(t, *a):
    return a[0] + a[1] * t + 0.5 * a[2] * t ** 2


def lin_vel_model(t, *a):
    return np.full(t.shape, a[0])


def quad_vel_model(t, *a):
    return a[0] + a[1] * t


# Function fitted in paper
def sin_vel_model(t, *a):
    return a[0] * np.sin(2 * np.pi * (1 / a[1]) * t + a[2]) + a[3] + a[4] * t


# Creating curve fit function called
def curvefunction(model, t, h, P0, lbounds, ubounds):
    try:
        opt, cov = curve_fit(model, t, h, p0=P0, bounds=(lbounds, ubounds))
    except RuntimeError as e:
        print(e)
        opt = np.full(5, np.inf)
    return opt

# Creating the sine fit for each CME
if __name__ == '__main__':
    n_runs = 1000
    n_bins = int(np.sqrt(n_runs))
    n_samaples = 100
    min_dt = datetime(1999, 3, 31, 4, 30, 4)
    max_dt = datetime(1999, 3, 31, 4, 30, 6)
    all_cmes = filter_cmes(min_dt, max_dt, 10)
    for i, cme in all_cmes.iterrows():
        res = []
        print("Processing CME {}".format(cme.datetime))
        ht_data = cme.loc['HT_DATA']
        times = ht_data.DATE_TIME.values
        heights = ht_data.HEIGHT.values
        
        # Error
        PerError = 0.3
        Errorh = heights * PerError - heights
        
        # Prepare data for fitting
        t = times.astype(float) * 1e-9  # Seconds
        t0 = t[0]
        t -= t0
        h = heights * RSUN  # Meters
        
        # Creating Quad Fit for initial conditions for Sine Fit
        lin_opt, lin_cov = curve_fit(lin_height_model, t, h, p0=[1, 1])
        quad_opt, quad_cov = curve_fit(quad_height_model, t, h, p0=[h[0], 1, 1], bounds=((0, 0, -1000), (30*RSUN, 4000e3, 1000)))

        t_diff, v = derivative(t, h)

        min_period = 60 * 60  # From paper
        
        # Create the Random starting positions for for sine wave
        amplitude = np.linspace(30e3, 400e3, n_samaples)
        phase = np.linspace(0, 2 * np.pi, n_samaples)
        period = np.linspace(min_period, t[-1], n_samaples)
        Velocity = np.linspace(0, 4000e3, n_samaples)
        Acceleration = np.linspace(-100, 100, n_samaples)
        
        # Creating the limits of the function
        lower_limits = (-np.inf, -np.inf, -np.inf, -np.inf, -np.inf)
        upper_limits = (np.inf, np.inf, np.inf, np.inf, np.inf)
        
        # Monte Carlo method to find best fit
        data = []

        for j in range(n_runs):
            # Create list of tuples

            i1, i2, i3, i4, i5 = np.random.choice(n_samaples, 5)
            initial_guess = (amplitude[i1], period[i3], phase[i2], Velocity[i4], Acceleration[i5])
            CurvefitValues = (sin_vel_model, t_diff, v, initial_guess, lower_limits, upper_limits)
            data.append(CurvefitValues)
        # Use Pool and curvefit to get all the parameters
        out = None
        with Pool(2*cpu_count()-2) as pool:
            out = pool.starmap(curvefunction, data)
            pool.close()
            pool.join()
        print("Done")
        res = np.array(out)
        # Sorting the list into an array and a better shape
        res = res.reshape((n_runs, 5))
        Counts = len(res[res==np.inf]) / 5
        # Save to File in data
        np.savez(os.path.join('..', 'data', 'Parameters', '{}'.format(cme.datetime)), res=res, Counts=Counts, n_bins=n_bins, t_diff=t_diff, v=v, quad_opt=quad_opt)
