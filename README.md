# README #

CME Velocity Osciallation analysis

### What is this repository for? ###

* SS project

### How do I get set up? ###

* Create a new environment (conda, virtualenv, pyenv)
* Run `pip install -r requirements.txt` inside new environment

### Devopers ###
* Run `pip install -r -devrequirements.txt`
* To run the tests `pytest `
* Copy the `hooks/pre-commit` file to you local `.git/hooks/` folder. This will run the relevant style checks and runt tests